# Ghost Of Ukraine

![Game Logo](/images/logo.png)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- 
## About the Game

Ghost Of Ukraine is a game created for support Ukrainian army. The game is made using Unity and C#.

![Game Screenshot 1](/images/screenshot1.png)

## Gameplay

Gameplay of game contains interesting mechanics. The game is made using Unity and C#.
Your game goal - destroy all tanks and vehicles from the Ghost Of Ukraine plane. 

## Features

Key features of game:
- Created to support Ukrainian army.
- Simple graphics and mechanics.

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/help_ukraine/ghost-of-ukraine.git`
2. Navigate to the game directory: `cd ghost-of-ukraine`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Swipe left/right to move plane.
- Tap to shoot.

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
