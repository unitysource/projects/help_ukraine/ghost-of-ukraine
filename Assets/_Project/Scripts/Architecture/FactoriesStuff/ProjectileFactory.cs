// using System;
// using _Project.Scripts.Services;
// using _Project.Scripts.Services.PoolStuff;
// using UnityEngine;
//
// namespace _Project.Scripts.FactoriesStuff
// {
//     public interface IProjectileFactory
//     {
//         void Load();
//         GameObject Create(ProjectileType projectileType, Vector3 at, Transform parent = null);
//
//         void Release(GameObject clone);
//     }
//
//     public class ProjectileFactory : IProjectileFactory
//     {
//         private const string TurretProjectile = "Projectiles/TurretProjectile";
//         private const string HeroProjectile = "Projectiles/HeroProjectile";
//         private const string EnemyProjectile = "Projectiles/EnemyProjectile";
//
//         private readonly AssetService _assetService;
//         private readonly IPoolService _poolService;
//
//         private GameObject _turretProjectilePrefab;
//         private GameObject _heroProjectilePrefab;
//         private GameObject _enemyProjectilePrefab;
//
//         public ProjectileFactory(AssetService assetService, IPoolService poolService)
//         {
//             _assetService = assetService;
//             _poolService = poolService;
//         }
//
//         public void Load()
//         {
//             _turretProjectilePrefab = _assetService.GetObjectByName(TurretProjectile);
//             _heroProjectilePrefab = _assetService.GetObjectByName(HeroProjectile);
//             _enemyProjectilePrefab = _assetService.GetObjectByName(EnemyProjectile);
//             
//             // _poolService.WarmPool(_turretProjectilePrefab, 10);
//             // _poolService.WarmPool(_heroProjectilePrefab, 10);
//         }
//
//         public GameObject Create(ProjectileType projectileType, Vector3 at, Transform parent = null)
//         {
//             GameObject projectile = projectileType switch
//             {
//                 ProjectileType.Turret => _poolService.GetPoolObject(_turretProjectilePrefab, at, Quaternion.identity,
//                     parent),
//                 ProjectileType.Hero => _poolService.SpawnObject(_heroProjectilePrefab, at, Quaternion.identity,
//                     parent),
//                 ProjectileType.Enemy => _poolService.SpawnObject(_enemyProjectilePrefab, at, Quaternion.identity,
//                     parent),
//                 _ => throw new ArgumentOutOfRangeException(nameof(projectileType), projectileType, null)
//             };
//             return projectile;
//         }
//
//         public void Release(GameObject clone)
//         {
//             // _poolService.ReleaseObject(clone);
//         }
//     }
//
//     public enum ProjectileType
//     {
//         Turret,
//         Hero,
//         Enemy
//     }
// }