using System;

namespace _Project.Scripts.Architecture.Services.SaveLoadService.Data
{
    [Serializable]
    public class PlayerData
    {
        public int Level
        {
            get => _level;
            set => _level = value;
        }

        public int ColumnIndex
        {
            get => _columnIndex;
            set => _columnIndex = value;
        }

        public int _level;
        public int _columnIndex;
    }
}