using _Project.Scripts.Architecture.SettingsStuff;
using _Project.Scripts.Services;
using Assets._Project.Scripts.Utilities.Constants;

namespace _Project.Scripts.SettingsStuff
{
    public class DataService
    {
        public WorldSettings WorldSettings { get; }
        public UISettings UISettings { get; }
        public PlanesSettings PlanesSettings { get; set; }

        public DataService(AssetService assetService)
        {
            WorldSettings = assetService.GetObjectByType<WorldSettings>(AssetPath.GlobalSettingsPath);
            PlanesSettings = assetService.GetObjectByType<PlanesSettings>(AssetPath.GlobalSettingsPath);
            UISettings = assetService.GetObjectByType<UISettings>(AssetPath.GlobalSettingsPath);
        }
    }
}