using _Project.Scripts.Services;
using UnityEngine;

public class StaticDataService : IStaticDataService
{
    private readonly AssetService _assetService;

    public StaticDataService(AssetService assetService)
    {
        _assetService = assetService;
    }

    public T GetStaticData<T>(string path) where T : ScriptableObject
    {
        return _assetService.Load<T>(path);
    }
}