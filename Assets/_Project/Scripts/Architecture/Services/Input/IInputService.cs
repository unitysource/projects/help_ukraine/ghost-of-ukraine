
using UnityEngine;

namespace _Project.Scripts.Infrastructure
{
    internal interface IInputService
    {
        Vector3 GetInput();
        bool IsInputAboveZero();
    }
}