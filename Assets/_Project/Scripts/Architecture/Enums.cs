namespace _Project.Scripts.Tiles
{
    public enum GameResourceType
    {
        Wood = 0,
        Stone = 1,
        Iron = 2,
        Gold = 3,
        Plank = 4,
        K_131 = 5,
        K_132 = 6,
        K_133 = 7,
        K_134 = 8,
        K_135 = 9,
        None = 100
    }
}