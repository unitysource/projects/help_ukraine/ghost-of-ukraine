using _Project.Scripts.FactoriesStuff;
using Zenject;

namespace _Project.Scripts.Infrastructure
{
    public class FactoriesInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindEntitiesFactory();
        }

        private void BindEntitiesFactory()
        {
            Container.Bind<IEntityFactory>().To<EntityPoolFactory>().AsSingle();
        }
    }
}