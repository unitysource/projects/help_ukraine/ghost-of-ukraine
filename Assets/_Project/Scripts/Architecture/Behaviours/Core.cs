﻿using System.Collections;
using _Core.Scripts.Managers.Save;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.Mechanics;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace _Project.Scripts
{
    public class Core : MonoBehaviour
    {
        [SerializeField] private Transform _uiCanvas;

        [Header("Sub Behaviours")] [SerializeField]
        private Cars _cars;
        [SerializeField] private CameraMover _cameraMover;
        [SerializeField] private BG _bg;
        [SerializeField] private Tutorial _tutorial;
        [SerializeField] private Attack _attack;
        [SerializeField] private CarCursor _carCursor;

        private IUIService _uiService;
        private DataService _dataService;
        private DiContainer _diContainer;
        private ISaveLoadService _saveLoadService;

        public Transform UICanvas => _uiCanvas;

        public bool CanFail { get; private set; } = true;

        [Inject]
        private void Construct(AssetService assetService, DataService dataService, IUIService uiService,
            DiContainer diContainer, ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
            _diContainer = diContainer;
            _dataService = dataService;
            _uiService = uiService;
            _uiService.Initialize(UICanvas);
        }

        private void Awake()
        {
            _bg.Construct(_dataService);
            _bg.Initialize();
            
            _carCursor.Initialize(0.3f);
            
            _cameraMover.Initialize(_carCursor);
            _cars.Initialize(this, null, _cameraMover);
            
            _attack.Construct(_uiService);
            _attack.Initialize(_cameraMover, _carCursor);

            InitializeUI();

            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void InitializeUI()
        {
            _uiService.OpenWindow(WindowType.TapToPlayWindow, true, false, true);
            _uiService.OpenWindow(WindowType.Pointer, true, false, true);
            _uiService.OpenWindow(WindowType.HUD, true, false, false);

            _uiService.AddListener(UIEventType.GameWinAction, Complete);
        }

        private void OnSceneUnloaded(Scene current)
        {
            QuitedCallback();
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        private void OnApplicationQuit()
        {
            QuitedCallback();
        }

        private void QuitedCallback()
        {
            CommonTools.SaveCurrentTime();
            SaveManager.SetString(SaveKey.SearchedCard, GameResourceType.None.ToString());
            _saveLoadService.SaveAllData();
        }

        private void Complete(Hashtable _)
        {
            CanFail = false;
            _saveLoadService.PlayerData.Level++;
            _uiService.OpenWindow(WindowType.DonatWindow, true, true, true);
        }
    }
}