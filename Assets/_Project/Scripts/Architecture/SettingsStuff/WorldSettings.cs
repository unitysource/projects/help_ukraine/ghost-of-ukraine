using UnityEngine;

namespace _Project.Scripts.Architecture.SettingsStuff
{
    [CreateAssetMenu(fileName = "WorldSettings", menuName = "Settings/WorldSettings")]
    public class WorldSettings : ScriptableObject
    {
        [SerializeField, Range(0.1f, 5f)] private float _bgMoveSpeed;
        
        [SerializeField, Range(10, 500)] private float _levelTime;
        [SerializeField, Range(1, 20)] private float _deltaLevelTime;

        [SerializeField] private int _failAttacksAmount;

        public float BgMoveSpeed => _bgMoveSpeed;

        public float LevelTime => _levelTime;

        public int FailAttacksAmount => _failAttacksAmount;

        public float DeltaLevelTime => _deltaLevelTime;
    }
}