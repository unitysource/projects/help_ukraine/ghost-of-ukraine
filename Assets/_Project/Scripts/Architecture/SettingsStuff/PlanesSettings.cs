﻿using _Project.Scripts.SettingsStuff;
using UnityEngine;

namespace _Project.Scripts.Architecture.SettingsStuff
{
    [CreateAssetMenu(fileName = "PlanesSettings", menuName = "Settings/PlanesSettings")]
    public class PlanesSettings : ScriptableObject
    {
        [Header("Hostile Properties")] [SerializeField, Range(0.01f, 5f)]
        private float _planeMoveSpeed;

        [SerializeField] private MinMaxFloat _createPlanesDelay;
        [SerializeField, Range(1, 15)] private float _increaseDangerSpeed;
        [SerializeField] private MinMaxFloat _yFlyBorders;

        [Header("Ghost Properties")] [SerializeField, Range(0.1f, 10f)]
        private float _ghostAttackDelay;

        [SerializeField, Range(0.1f, 10f)] private float _moveToTargetDuration;
        [SerializeField, Range(0.1f, 10f)] private float _projectileMoveSpeed;

        public float PlaneMoveSpeed => _planeMoveSpeed;
        public float GhostAttackDelay => _ghostAttackDelay;
        public float MoveToTargetDuration => _moveToTargetDuration;
        public float ProjectileMoveSpeed => _projectileMoveSpeed;

        public MinMaxFloat CreatePlanesDelay => _createPlanesDelay;

        public float IncreaseDangerSpeed => _increaseDangerSpeed;

        public MinMaxFloat YFlyBorders => _yFlyBorders;
    }
}