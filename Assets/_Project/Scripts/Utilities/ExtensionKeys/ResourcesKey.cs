namespace Assets._Project.Scripts.Utilities.ExtensionKeys
{
    public class ResourcesKey
    {
        public string Key { get; }
        private ResourcesKey(string key) => Key = key;
        public static implicit operator ResourcesKey(string key) => new ResourcesKey(key);
    }
}