﻿using System;

namespace _Project.Scripts.Mechanics
{
    public static class StateManager
    {
        public static void EnterAction(ref bool isEntered, Action action = null)
        {
            if (isEntered) return;

            isEntered = true;
            action?.Invoke();
        }

        public static void ChangeState<T>(ref (T, bool) state, T stateElement)
        {
            state.Item1 = stateElement;
            state.Item2 = false;
        }
    }
}