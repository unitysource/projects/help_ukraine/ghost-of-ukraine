using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.Utilities.Tools;
using Assets._Project.Scripts.Utilities;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class HUDWindow : WindowBase
    {
        [SerializeField] private Button _openOptionsButton;

        [Header("Timer")] [SerializeField] private Image _timerProgressImage;
        [SerializeField] private TMP_Text _timerProgressTMP;
        [SerializeField] private TMP_Text _downedPlanesTMP;
        
        private float _levelTime;
        private ISaveLoadService _saveLoadService;
        private Tween _progressImageTween;
        private Tween _progressTMPTween;

        [Inject]
        private void Construct(ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
        }

        public override void Initialize()
        {
            base.Initialize();
            _openOptionsButton.onClick.AddListener(OpenOptions);

            _timerProgressImage.fillAmount = 1f;
            _levelTime = _dataService.WorldSettings.LevelTime +
                         _dataService.WorldSettings.DeltaLevelTime * _saveLoadService.PlayerData.Level;
            UITools.UpdateText(_timerProgressTMP, $"{_levelTime}");

            UITools.UpdateText(_downedPlanesTMP, "0");

            _uiService.AddListener(UIEventType.CarRemovedAction, OnPlaneRemoved);
        }

        private void OnGameFinished(Hashtable arg0)
        {
            _progressImageTween.Pause();
            _progressTMPTween.Pause();
        }

        private void OnPlaneRemoved(Hashtable downedPlanesAmount)
        {
            UITools.UpdateText(_downedPlanesTMP, $"{downedPlanesAmount.GetFirstValue()}");
        }

        protected override void OnOpenedState()
        {
            _progressTMPTween = UITools.CountdownTimer(_timerProgressTMP, (int) _levelTime, 0, _levelTime,
                () => _uiService.TriggerEvent(UIEventType.GameWinAction));
            _progressImageTween = UITools.CountdownProgress(_timerProgressImage, _levelTime);
        }

        private void OpenOptions()
        {
            _uiService.OpenWindow(WindowType.Pause, true, true, true);
            CloseWindow(false);
        }

        public override WindowType GetWindowType() => WindowType.HUD;
    }
}