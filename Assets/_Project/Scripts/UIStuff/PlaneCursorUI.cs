﻿// using _Project.Scripts.Architecture.Services;
// using _Project.Scripts.Mechanics;
// using UnityEngine;
// using UnityEngine.UI;
//
// namespace _Project.Scripts.UIStuff
// {
//     public class PlaneCursorUI : MonoBehaviour
//     {
//         [SerializeField] private Button _cursorButton;
//         private PlaneCursorController _planeCursor;
//
//         public void Initialize(PlaneCursorController planeCursor)
//         {
//             _planeCursor = planeCursor;
//
//             _cursorButton.onClick.AddListener(CursorClicked);
//         }
//
//         private void CursorClicked() => _planeCursor.CursorClicked();
//     }
// }