﻿using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Services;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class Lines : MonoBehaviour
    {
        [SerializeField] private int _linesAmount;

        private List<Line> _lines;
        private float _screenWidth;
        private RandomService _randomService;

        [Inject]
        private void Construct(RandomService randomService)
        {
            _randomService = randomService;
        }

        public void Initialize()
        {
            _lines = new List<Line>(_linesAmount);

            _screenWidth = CommonTools.GetScreenValues().y;
        }

        public void CreateLines()
        {
            for (int x = -3; x < 4; x += 2)
            {
                if (x == 0) continue;

                var newLine = new Line
                {
                    Position = (_screenWidth / 2) / _linesAmount * x,
                    IsEmpty = true
                };

                _lines.Add(newLine);
            }
        }

        public Line GetRandomAvailableLine()
        {
            List<Line> availableLines = _lines.Where(l => l.IsEmpty).ToList();
            int value = _randomService.GetValue(0, availableLines.Count);
            Line line = availableLines[value];
            line.IsEmpty = false;
            return line;
        }

        public bool HaveAvailableLine => _lines.Any(l => l.IsEmpty);
    }
}