﻿using System;
using System.Collections;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class CameraMover : MonoBehaviour, IGizmos
    {
        [SerializeField] private Swiper _swiper;
        [SerializeField] private float _maxSwiperMoveSpeed;
        [SerializeField] private float _swiperDecreaseValue;

        [SerializeField] private Transform _cameraHolder;

        // [SerializeField] private Transform _cameraHolderSwiper;
        [SerializeField] private float _maxBordersOffset;
        [SerializeField] private float _moveSpeed;

        public bool CanMove { get; private set; }
        public Vector2 BoardPosition => new Vector2(_transform.position.x, _transform.position.y);
        public Vector2 CursorPosition => _carCursor.transform.position;


        // private Camera _camera;
        private RandomService _randomService;
        private Vector2 _targetPosition;
        private Borders _borders;
        private Transform _transform;
        private GizmosService _gizmosService;
        private const float ReachedDelta = 0.2f;
        private Vector2 _startPosition;
        private IUIService _uiService;
        private Vector3 _clampedDelta;
        private CarCursor _carCursor;

        [Inject]
        private void Construct(RandomService randomService, GizmosService gizmosService, IUIService uiService)
        {
            _uiService = uiService;
            _gizmosService = gizmosService;
            _randomService = randomService;
        }

        public void Initialize(CarCursor carCursor)
        {
            _carCursor = carCursor;
            _swiper.Initialize(this);
            _gizmosService.AddGizmo(this);

            // _camera = Camera.main;
            _transform = _cameraHolder.transform;
            float borderValue = _maxBordersOffset / 2;
            _borders = new Borders(MinMaxFloat.Create(-borderValue, borderValue),
                MinMaxFloat.Create(-borderValue, borderValue));
            _startPosition = _transform.position;

            _uiService.AddListener(UIEventType.GameStartedAction, OnGameStarted);
        }

        private void OnGameStarted(Hashtable _)
        {
            CanMove = true;
        }

        private void FixedUpdate()
        {
            if (CanMove)
            {
                if (TargetPointReached)
                    SetNextPoint();
                MoveToPoint();
            }
        }

        private void MoveToPoint()
        {
            float step = _moveSpeed / 10 * Time.deltaTime;
            _transform.position =
                Vector3.MoveTowards(_transform.position, _targetPosition, step);
        }

        private float _distanceToTarget => Vector2.Distance(_transform.position, _targetPosition);

        private bool TargetPointReached =>
            _distanceToTarget <= ReachedDelta;

        private void SetNextPoint() => _targetPosition = _randomService.GetValue(_borders);

        public void PointerDown()
        {
            _moveSpeed /= _swiperDecreaseValue;
        }

        public void PointerUp()
        {
            _moveSpeed *= _swiperDecreaseValue;
        }

        public void Drag(Vector2 moveDelta)
        {
            var maxSwiperMoveSpeed = _maxSwiperMoveSpeed / 50;
            _clampedDelta = new Vector3(Mathf.Clamp(moveDelta.x, -maxSwiperMoveSpeed, maxSwiperMoveSpeed),
                Mathf.Clamp(moveDelta.y, -maxSwiperMoveSpeed, maxSwiperMoveSpeed));
            _cameraHolder.transform.position += _clampedDelta;
        }

        public void DrawGizmo()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(Vector3.zero, new Vector2(_maxBordersOffset, _maxBordersOffset));
            Gizmos.DrawSphere(_targetPosition, 1f);
        }
    }
}