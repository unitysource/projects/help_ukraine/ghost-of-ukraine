﻿// using System;
// using System.Collections;
// using _Project.Scripts.Architecture.Services.SaveLoadService;
// using _Project.Scripts.Architecture.Services.UIStuff;
// using DG.Tweening;
// using UnityEngine;
//
// namespace _Project.Scripts.Mechanics
// {
//     public class Levels : MonoBehaviour
//     {
//         private ISaveLoadService _saveLoadService;
//         private Cars _cars;
//         private CameraMover _cameraMover;
//         private IUIService _uiService;
//
//         public void Construct(ISaveLoadService saveLoadService, IUIService uiService)
//         {
//             _uiService = uiService;
//             _saveLoadService = saveLoadService;
//         }
//
//         public void Initialize(Cars cars, CameraMover cameraMover)
//         {
//             _cars = cars;
//             _cameraMover = cameraMover;
//
//             _uiService.AddListener(UIEventType.CarRemovedAction, OnCarsRemoved);
//         }
//     }
// }