﻿namespace _Project.Scripts.Mechanics
{
    public class Line
    {
        public bool IsEmpty { get; set; }
        public float Position { get; set; }
    }
}