﻿using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class Rotator : MonoBehaviour
    {
        [SerializeField] private float _duration;

        private void Start()
        {
            transform.DOLocalRotate(Vector3.forward * 360, _duration, RotateMode.FastBeyond360)
                .SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        }

        public void StopRotate() => transform.DOKill();
    }
}