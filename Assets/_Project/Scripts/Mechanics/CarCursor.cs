﻿using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class CarCursor : MonoBehaviour
    {
        [SerializeField] private Color[] _attackColor;
        
        private float _scaleAnimateDelay;
        private Transform _transform;
        private Vector3 _defaultScale;
        private SpriteRenderer _spriteRenderer;
        private Color _defaultColor;

        public void Initialize(float scaleAnimateDelay)
        {
            _transform = transform;
            _defaultScale = _transform.localScale;
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _scaleAnimateDelay = scaleAnimateDelay;
            _defaultColor = _spriteRenderer.color;
        }

        public void BeginScaleAnimate()
        {
            _transform.DOKill();
            _spriteRenderer.DOKill();

            _transform.DOScale(_defaultScale * 1.6f, _scaleAnimateDelay).onComplete += () => 
                _transform.DOScale(_defaultScale * 1.3f, _scaleAnimateDelay).SetLoops(-1, LoopType.Yoyo);

            const float minFade = 0.5f;
            const float maxFade = 1f;
            _spriteRenderer.DOFade(maxFade, _scaleAnimateDelay).From(minFade)
                .SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
        }

        public void StopScaleAnimate()
        {
            _transform.DOKill();
            _spriteRenderer.DOKill();

            _transform.DOScale(_defaultScale, _scaleAnimateDelay);
        }

        public void Attack()
        {
            _spriteRenderer.DOKill();
            const float duration = 0.3f;
            _spriteRenderer.DOColor(_attackColor[0], duration).onComplete += () =>
                _spriteRenderer.DOColor(_attackColor[1], duration);
        }
        
        public void DeAttack()
        {
            _spriteRenderer.DOKill();
            _spriteRenderer.DOColor(_defaultColor, 0.3f);
        }
    }
}