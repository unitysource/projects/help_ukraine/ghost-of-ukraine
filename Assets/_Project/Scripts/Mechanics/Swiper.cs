﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Mechanics
{
    public class Swiper : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        private CameraMover _cameraMover;

        public void Initialize(CameraMover cameraMover)
        {
            _cameraMover = cameraMover;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _cameraMover.PointerDown();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _cameraMover.Drag(eventData.delta);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _cameraMover.PointerUp();
        }
    }
}