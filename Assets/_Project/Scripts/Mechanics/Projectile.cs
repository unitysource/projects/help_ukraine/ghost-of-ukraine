﻿using System;
using _Project.Scripts.SettingsStuff;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _trailParticles;
        [SerializeField] private float _generateTrailDelay;

        private Transform _transform;
        private float _moveSpeed;
        private DataService _dataService;

        public void Construct(DataService dataService)
        {
            _dataService = dataService;
        }

        public void Initialize()
        {
            _transform = transform;
            _moveSpeed = _dataService.PlanesSettings.ProjectileMoveSpeed;

            DOVirtual.DelayedCall(_generateTrailDelay, () => _trailParticles.Play());
        }

        private void FixedUpdate()
        {
            _transform.position += Vector3.up * (_moveSpeed * Time.deltaTime);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.TryGetComponent<Car>(out _)) 
                Explode();
        }

        private void Explode() => Destroy(gameObject);
    }
}