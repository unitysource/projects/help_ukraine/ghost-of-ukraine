﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.SettingsStuff;
using Assets._Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class BG : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _tileSpriteRendererPrefab;
        [SerializeField] private Transform _tilesParent;
        [SerializeField] private Transform _buildingsParent;

        private readonly List<SpriteRenderer> _tileSpriteRenderers = new List<SpriteRenderer>();
        private float _screenWidth;
        private Vector2 _tilesStartPoint;
        private float _speed;
        private DataService _dataService;
        private int _tempTileIndex;
        private float _changePositionPoint;
        private Vector2 _screenValues;

        private float _tileSpriteScale;
        private Borders _borders = new Borders();
        private float _tileScale;
        // private const int _capacity = 5;

        public void Construct(DataService dataService)
        {
            _dataService = dataService;
        }

        public void Initialize()
        {
            _screenValues = CommonTools.GetScreenValues();
            _screenWidth = _screenValues.y;
            _speed = _dataService.WorldSettings.BgMoveSpeed;

            InitializeTiles();
            CreateTiles();
        }

        private void InitializeTiles()
        {
            var sprite = _tileSpriteRendererPrefab.sprite;
            float width = sprite.bounds.size.x;
            _tileScale = _screenWidth * 2;
            _tileSpriteScale = _tileScale / width;
            _tilesStartPoint = Vector3.zero;
            _tempTileIndex = 0;
            _changePositionPoint = _tilesStartPoint.x + _tileSpriteScale;
        }

        private void CreateTiles()
        {
            for (int x = -4; x < 5; x++)
            {
                for (int y = -4; y < 5; y++)
                {
                    var tileSpriteRenderer = Instantiate(_tileSpriteRendererPrefab, _tilesParent);
                    tileSpriteRenderer.transform.position = new Vector2(_tilesStartPoint.x + x * _tileScale,
                        _tilesStartPoint.y + y * _tileScale);
                    SetScale(tileSpriteRenderer, _tileSpriteScale);
                    _tileSpriteRenderers.Add(tileSpriteRenderer);
                }
            }
        }

        private void FixedUpdate()
        {
            MoveBG();

            // foreach (var tempTile in _tileSpriteRenderers)
            // {
            //     if (tempTile.transform.position.y <= _borders)
            //     {
            //         tempTile.transform.position = new Vector3(0, _tilesStartPoint + _capacity * _screenWidth, 0);
            //         SetTempTileIndex();
            //     }
            // }
        }

        private void MoveBG()
        {
        }

        private void SetScale(SpriteRenderer tileSpriteRenderer, float tileSpriteScale) =>
            tileSpriteRenderer.transform.localScale = Vector3.one * tileSpriteScale * 1.02f;
    }
}