﻿using System;
using System.Collections;
using _Project.Scripts.Architecture.Services.UIStuff;
using JetBrains.Annotations;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class Attack : MonoBehaviour
    {
        [SerializeField] private float _retentionDelay;

        private CameraMover _cameraMover;
        private CarCursor _carCursor;
        private float _checkTimer;
        private (AttackState, bool) _attackState;
        private IUIService _uiService;

        public void Construct(IUIService uiService)
        {
            _uiService = uiService;
        }

        public void Initialize(CameraMover cameraMover, CarCursor carCursor)
        {
            _cameraMover = cameraMover;
            _carCursor = carCursor;
            _attackState = (AttackState.None, false);

            _uiService.AddListener(UIEventType.GameStartedAction, OnGameStarted);
        }

        private void OnGameStarted(Hashtable arg0)
        {
            StateManager.ChangeState(ref _attackState, AttackState.TimerCheck);
        }

        private void Update() => 
            StatesHandler();

        private void StatesHandler()
        {
            switch (_attackState.Item1)
            {
                case AttackState.None:
                    break;
                case AttackState.Search:
                    StateManager.EnterAction(ref _attackState.Item2, () =>
                    {
                        _carCursor.StopScaleAnimate();
                        _carCursor.DeAttack();
                    });
                    RaycastCheck(
                        () => { StateManager.ChangeState(ref _attackState, AttackState.TimerCheck); }, null, out _);
                    break;
                case AttackState.TimerCheck:
                    StateManager.EnterAction(ref _attackState.Item2, () => {
                    {
                        _carCursor.BeginScaleAnimate();
                        _carCursor.Attack();
                    } });

                    RaycastCheck(
                        () => { _checkTimer += Time.deltaTime; },
                        () => { StateManager.ChangeState(ref _attackState, AttackState.Search); }, out _);

                    if (_checkTimer >= _retentionDelay)
                    {
                        _checkTimer = 0f;
                        StateManager.ChangeState(ref _attackState, AttackState.Attack);
                    }

                    break;
                case AttackState.Attack:
                    StateManager.EnterAction(ref _attackState.Item2, () =>
                    {
                        RaycastCheck(null, null, out var car);
                        car.GetDamage();
                        StateManager.ChangeState(ref _attackState, AttackState.Search);
                    });
                    break;
            }
        }

        private void RaycastCheck([CanBeNull] Action find, [CanBeNull] Action notFind, out Car car)
        {
            car = null;
            // Debug.DrawLine(_cameraMover.BoardPosition, _cameraMover.BoardPosition);
            RaycastHit2D hit = Physics2D.Raycast(_cameraMover.CursorPosition, Vector2.down, 0.1f);
            Collider2D hitCollider = hit.collider;
            if (hitCollider != null && hitCollider.TryGetComponent<Car>(out var targetCar))
            {
                car = targetCar;
                find?.Invoke();
            }
            else
            {
                notFind?.Invoke();
            }
        }
    }

    public enum AttackState
    {
        None,
        Search,
        TimerCheck,
        Attack
    }
}