﻿namespace _Project.Scripts.Mechanics
{
    public enum CarsSchemeType
    {
        One = 0,
        TwoVertical = 1,
        TwoHorizontal = 2,
        ThreeVertical = 3,
        ThreeHorizontal = 4,
        ThreeTriangle = 5,
        FourSquare = 6,
        FourRhombus = 7,
        Five = 8,
        Six = 9,
        Seven = 10
    }
}