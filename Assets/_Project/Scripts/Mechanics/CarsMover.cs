﻿using System.Collections;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class CarsMover : MonoBehaviour, IGizmos
    {
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _maxCarsOffsetFromCamera;

        private IUIService _uiService;
        private GizmosService _gizmosService;
        private RandomService _randomService;
        private Borders _borders = new Borders();
        private Vector2 _targetPosition;
        private Transform _carsParent;
        private CameraMover _cameraMover;
        private Cars _cars;
        public bool CanMove { get; set; }
        private const float _spriteOffset = -90;
        
        public Borders CarsBorders => _borders;

        [Inject]
        public void Construct(RandomService randomService, GizmosService gizmosService, IUIService uiService)
        {
            _uiService = uiService;
            _gizmosService = gizmosService;
            _randomService = randomService;
        }

        public void Initialize(CameraMover cameraMover, Cars cars)
        {
            _cars = cars;
            _cameraMover = cameraMover;
            _carsParent = cars.CarsParent;

            _gizmosService.AddGizmo(this);
            _uiService.AddListener(UIEventType.GameStartedAction, OnGameStarted);
        }

        private void OnGameStarted(Hashtable _)
        {
            CanMove = true;
        }

        private void FixedUpdate()
        {
            if (CanMove)
            {
                if (TargetPointReached)
                {
                    SetBorders();
                    SetNextPoint();
                }
                
                MoveToPoint();
            }
        }

        private void SetBorders()
        {
            Vector2 cameraPos = _cameraMover.BoardPosition;
            _borders = Borders.Create(
                MinMaxFloat.Create(cameraPos.x - _maxCarsOffsetFromCamera / 2,
                    cameraPos.x + _maxCarsOffsetFromCamera / 2),
                MinMaxFloat.Create(cameraPos.y - _maxCarsOffsetFromCamera / 1.5f,
                    cameraPos.y + _maxCarsOffsetFromCamera / 1.5f));
        }

        private void SetNextPoint() => _targetPosition = _randomService.GetValue(_borders);

        private void MoveToPoint()
        {
            _carsParent.DOKill();
            CarsRotationHandler();
            
            float step = _moveSpeed / 10 * Time.deltaTime;
            _carsParent.position =
                Vector3.MoveTowards(_carsParent.position, _targetPosition, step);
        }

        public void CarsRotationHandler()
        {
            foreach (var car in _cars.TempColumn)
            {
                car.TrackRenderer.DOKill();
                car.TrackRenderer.DOFade(0f, 0.05f);
                var direction = _targetPosition - (Vector2) _carsParent.position;
                direction.Normalize();
                var zAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                car.transform.DOLocalRotate(Vector3.forward * (zAngle + _spriteOffset), 0.3f).onComplete +=
                    () => car.TrackRenderer.DOFade(1f, 0.2f);
            }
        }

        private float _distanceToTarget => Vector2.Distance(_carsParent.position, _targetPosition);

        private bool TargetPointReached =>
            _distanceToTarget <= ReachedDelta;

        private const float ReachedDelta = 0.2f;

        public void DrawGizmo()
        {
            Gizmos.color = Color.red;
            CameraMover mover = FindObjectOfType<CameraMover>();
            if (mover.CanMove)
            {
                Gizmos.DrawWireCube(mover.BoardPosition,
                    new Vector2(_maxCarsOffsetFromCamera, _maxCarsOffsetFromCamera * 1.333f));
                Gizmos.DrawSphere(_targetPosition, 1f);
            }
        }
    }
}