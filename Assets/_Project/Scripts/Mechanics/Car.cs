﻿using System;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class Car : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private GameObject _explosionPrefab;
        [SerializeField] private SpriteRenderer _lightsRenderer;
        [SerializeField] private SpriteRenderer _trackRenderer;

        private (CarState, bool) _carState;

        public bool IsTarget { get; set; }
        private const float ReachedDelta = 0.2f;

        private DataService _dataService;
        private RandomService _randomService;
        private Transform _transform;
        private float _moveSpeed;
        private MinMaxFloat _yBorders;
        private Cars _cars;
        private bool _isKilled;
        public CarSettings carSettings { get; set; }

        public SpriteRenderer TrackRenderer => _trackRenderer;

        public void Construct(DataService dataService, RandomService randomService, Cars cars)
        {
            _cars = cars;
            _randomService = randomService;
            _dataService = dataService;
        }

        public void Initialize(CarSettings carSettings)
        {
            this.carSettings = carSettings;
            _moveSpeed = _dataService.PlanesSettings.PlaneMoveSpeed;
            _yBorders = _dataService.PlanesSettings.YFlyBorders;
            _transform = transform;
            _carState = (CarState.Start, false);
        }

        private void Update()
        {
            switch (_carState.Item1)
            {
                case CarState.Start:
                    break;
                case CarState.Move:
                    break;
                case CarState.GetDamage:
                    break;
                case CarState.Fail:
                    break;
            }
        }

        private void Explode()
        {
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        public void GetDamage()
        {
            if (_isKilled) return;
            _isKilled = true;

            const float duration = 0.3f;
            _spriteRenderer.DOKill();

            _spriteRenderer.DOFade(0.9f, duration).onComplete += () =>
            {
                _lightsRenderer.DOFade(0f, duration);
                _spriteRenderer.DOFade(0.2f, duration).onComplete += () =>
                {
                    _cars.RemoveCar(this);
                    Explode();
                };
            };
        }
    }

    public enum CarState
    {
        Start,
        Move,
        GetDamage,
        Fail
    }
}