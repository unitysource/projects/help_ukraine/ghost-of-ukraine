﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using ModestTree;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class Cars : MonoBehaviour
    {
        [SerializeField] private CarSettings[] _cars;
        [SerializeField] private Car _carPrefab;
        [SerializeField] private float _creationOffset;
        [SerializeField] private Transform _carsParent;
        [SerializeField] private CarsMover _carsMover;

        private IPoolService _poolService;
        private DataService _dataService;
        private RandomService _randomService;
        private IUIService _uiService;
        private Core _core;
        private Tutorial _tutorial;
        private string _prefabName;
        private int DownedPlanesAmount;
        private int _attacksCounter;
        private CameraMover _cameraMover;
        private ISaveLoadService _saveLoadService;

        private const string ContainerName = "CarsContainer";

        public List<Car> TempColumn { get; } = new List<Car>(30);

        public Transform CarsParent => _carsParent;

        [Inject]
        public void Construct(IPoolService poolService, IUIService uiService, DataService dataService,
            RandomService randomService, ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
            _randomService = randomService;
            _uiService = uiService;
            _dataService = dataService;
            _poolService = poolService;
        }

        public void Initialize(Core core, Tutorial tutorial, CameraMover cameraMover)
        {
            _cameraMover = cameraMover;
            _tutorial = tutorial;
            _core = core;
            FillPool();

            _carsMover.Initialize(_cameraMover, this);

            _uiService.AddListener(UIEventType.GameStartedAction, OnGameStarted);
            _uiService.AddListener(UIEventType.GameWinAction, OnGameFinished);
        }

        private void FillPool() =>
            _poolService.FillPool(PoolInfo.Create(_prefabName, 100, _carPrefab.gameObject, ContainerName));

        private void CreateCarsByScheme(Vector2 at, CarsSchemeType scheme, CarSettings carSettings = null)
        {
            TempColumn.Clear();

            var randCarSettings = carSettings;
            var randCarsSettings = new List<CarSettings>();
            int index = 0;

            switch (scheme)
            {
                case CarsSchemeType.One:
                    if (carSettings == null)
                        randCarSettings = _cars.GetRandomElement();

                    CreateCar(randCarSettings, at);
                    break;
                case CarsSchemeType.TwoVertical:
                    for (int i = -1; i < 2; i++)
                    {
                        if (i == 0) continue;

                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();
                        CreateCar(randCarSettings, new Vector2(at.x, at.y + _creationOffset * i * 0.7f));
                    }

                    break;
                case CarsSchemeType.TwoHorizontal:
                    for (int i = -1; i < 2; i++)
                    {
                        if (i == 0) continue;

                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();
                        CreateCar(randCarSettings, new Vector2(at.x + _creationOffset / 2 * i, at.y));
                    }

                    break;
                case CarsSchemeType.ThreeVertical:
                    for (int i = -1; i < 2; i++)
                    {
                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();
                        CreateCar(randCarSettings, new Vector2(at.x, at.y + _creationOffset * 1.5f * i));
                    }

                    break;
                case CarsSchemeType.ThreeHorizontal:
                    for (int i = -1; i < 2; i++)
                    {
                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();
                        CreateCar(randCarSettings, new Vector2(at.x + _creationOffset * i, at.y));
                    }

                    break;
                case CarsSchemeType.ThreeTriangle:
                    for (int i = 0; i < 3; i++)
                        randCarsSettings.Add(carSettings == null ? _cars.GetRandomElement() : carSettings);

                    CreateCar(randCarsSettings[index], new Vector2(at.x, at.y + _creationOffset / 2));
                    CreateCar(randCarsSettings[++index],
                        new Vector2(at.x - _creationOffset / 2, at.y - _creationOffset / 2));
                    CreateCar(randCarsSettings[++index],
                        new Vector2(at.x + _creationOffset / 2, at.y - _creationOffset / 2));
                    break;
                case CarsSchemeType.FourSquare:
                    for (int i = -1; i < 2; i++)
                    {
                        if (i == 0) continue;

                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();
                        CreateCar(randCarSettings,
                            new Vector2(at.x + _creationOffset / 2 * i, at.y - _creationOffset * 0.7f));
                        CreateCar(randCarSettings,
                            new Vector2(at.x + _creationOffset / 2 * i, at.y + _creationOffset * 0.7f));
                    }

                    break;
                case CarsSchemeType.FourRhombus:
                    for (int i = -1; i < 2; i++)
                    {
                        if (i == 0) continue;

                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();
                        CreateCar(randCarSettings, new Vector2(at.x + _creationOffset / 2 * i, at.y));
                        CreateCar(randCarSettings, new Vector2(at.x, at.y + _creationOffset * 1.5f * i));
                    }

                    break;
                case CarsSchemeType.Five:
                    for (int i = -1; i < 2; i++)
                    {
                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();

                        if (i == 0)
                        {
                            CreateCar(randCarSettings, at);
                        }
                        else
                        {
                            CreateCar(randCarSettings, new Vector2(at.x + _creationOffset * i, at.y - _creationOffset));
                            CreateCar(randCarSettings, new Vector2(at.x + _creationOffset * i, at.y + _creationOffset));
                        }
                    }

                    break;
                case CarsSchemeType.Six:
                    for (int i = -1; i < 2; i++)
                    {
                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();

                        CreateCar(randCarSettings,
                            new Vector2(at.x + _creationOffset, at.y + _creationOffset * i * 1.5f));
                        CreateCar(randCarSettings,
                            new Vector2(at.x - _creationOffset, at.y + _creationOffset * i * 1.5f));
                    }

                    break;
                case CarsSchemeType.Seven:
                    for (int i = -1; i < 2; i++)
                    {
                        if (carSettings == null)
                            randCarSettings = _cars.GetRandomElement();

                        if (i == 0) CreateCar(randCarSettings, at);

                        CreateCar(randCarSettings, new Vector2(at.x + _creationOffset * i, at.y - _creationOffset));
                        CreateCar(randCarSettings, new Vector2(at.x + _creationOffset * i, at.y + _creationOffset));
                    }

                    break;
            }
        }

        private void OnGameFinished(Hashtable arg0)
        {
        }

        private void OnGameStarted(Hashtable arg0)
        {
            CreateNewColumn(false);
        }

        private void Update()
        {
            // if (Input.GetKeyDown(KeyCode.D))
            // {
            //     foreach (var car in TempColumn)
            //         car.GetDamage();
            // }

            // if (Input.GetKeyDown(KeyCode.Space))
            // {
            //     Vector2 startPos = Vector2.right * 10;
            //     CarsSchemeType[] carsSchemeTypes = (CarsSchemeType[]) Enum.GetValues(typeof(CarsSchemeType));
            //     int length = EnumTools.GetLength(typeof(CarsSchemeType));
            //     for (int i = 0; i < length; i++)
            //         CreateCarsByScheme(startPos + Vector2.right * (i * 7), carsSchemeTypes[i]);
            // }
        }

        private void CreateCar(CarSettings carSettings, Vector2 at)
        {
            Car car = _poolService.GetPoolObject(_prefabName).GetComponent<Car>();
            car.transform.SetParent(CarsParent);
            car.gameObject.SetActive(true);
            car.transform.position = at;
            car.Construct(_dataService, _randomService, this);
            car.Initialize(carSettings);
            AddCar(car);
        }

        private void AddCar(Car car) => TempColumn.Add(car);

        public void RemoveCar(Car car)
        {
            TempColumn.Remove(car);

            if (TempColumn.IsEmpty())
            {
                DOVirtual.DelayedCall(0.5f, () => CreateNewColumn(true));
            }

            DownedPlanesAmount += car.carSettings.PointsAmount;
            _uiService.TriggerEvent(UIEventType.CarRemovedAction,
                new Hashtable {{"DownedPlanesAmount", DownedPlanesAmount}});
        }

        private void CreateNewColumn(bool needIncrease)
        {
            var columnIndex = _saveLoadService.PlayerData.ColumnIndex;
            int enumLength = EnumTools.GetLength(typeof(CarsSchemeType));
            bool realCol = columnIndex < enumLength;
            
            if (realCol && needIncrease)
            {
                _saveLoadService.PlayerData.ColumnIndex++;
                _saveLoadService.SaveAllData();
            }
            
            CreateCarsByScheme(_randomService.GetValue(_carsMover.CarsBorders),
                realCol
                    ? (CarsSchemeType) _saveLoadService.PlayerData.ColumnIndex
                    : EnumTools.GetRandom<CarsSchemeType>(typeof(CarsSchemeType)));
            _carsMover.CarsRotationHandler();
        }
    }
}