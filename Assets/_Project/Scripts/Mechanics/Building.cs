﻿using System;
using System.Security.Cryptography;
using _Project.Scripts.Services;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class Building : MonoBehaviour
    {
        [SerializeField] private BuildingSprites[] _buildingSprites;
        [SerializeField] private GameObject _explosionPrefab;

        private SpriteRenderer _spriteRenderer;
        private int _randomIndex;
        private const float Duration = 0.3f;
        private RandomService _randomService;

        public void Initialize(RandomService randomService)
        {
            _randomService = randomService;
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _randomIndex = _randomService.GetValue(0, _buildingSprites.Length);
            _spriteRenderer.sprite = _buildingSprites[_randomIndex].WholeBuilding;
        }

        public void Appear()
        {
            _spriteRenderer.DOFade(1f, Duration).From(0f);
        }

        public void Brake()
        {
            var explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            _spriteRenderer.DOFade(0f, Duration).onComplete += () =>
            {
                _spriteRenderer.sprite = _buildingSprites[_randomIndex].BrakedBuilding;
                _spriteRenderer.DOFade(1f, Duration).onComplete += () =>
                {
                    Destroy(explosion);
                    _spriteRenderer.DOFade(0f, Duration).onComplete += () =>
                        Destroy(gameObject);
                };
            };
        }
    }

    [Serializable]
    public class BuildingSprites
    {
        [SerializeField] private Sprite _wholeBuilding;
        [SerializeField] private Sprite _brakedBuilding;

        public Sprite BrakedBuilding => _brakedBuilding;

        public Sprite WholeBuilding => _wholeBuilding;
    }
}