﻿using _Project.Scripts.Services;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class PlaneAttack : MonoBehaviour
    {
        [SerializeField] private Building _buildingPrefab;
        [SerializeField] private SpriteRenderer _bombPrefab;

        private RandomService _randomService;
        private Building _newBuilding;
        private Transform _bg;

        [Inject]
        private void Construct(RandomService randomService)
        {
            _randomService = randomService;
        }

        public void Initialize(Transform bg)
        {
            _bg = bg;
        }

        public void CreateBuilding(Vector2 at)
        {
            _newBuilding = Instantiate(_buildingPrefab, at, Quaternion.identity);
            _newBuilding.transform.SetParent(_bg.transform);
            _newBuilding.Initialize(_randomService);
            _newBuilding.Appear();
        }

        public void Attack(Vector2 at)
        {
            const float duration = 0.3f;
            var bomb = Instantiate(_bombPrefab, at, Quaternion.identity);
            bomb.transform.DOScale(0.5f, duration).onComplete += () =>
            {
                Destroy(bomb.gameObject);
                _newBuilding.Brake();
            };
        }
    }
}