﻿using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class Tutorial : MonoBehaviour
    {
        [SerializeField] private Transform _pointer;
        private SpriteRenderer _spriteRenderer;

        public void Initialize()
        {
            _spriteRenderer = _pointer.GetComponentInChildren<SpriteRenderer>();
        }

        public void BeginPlay()
        {
            _pointer.DOMove(new Vector2(0f, -3.1f), 0.5f).onComplete += () =>
            {
                _pointer.DOScale(1.15f, 0.3f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.OutCirc);
            };
        }

        public void Deactivate()
        {
            IsActive = false;
            _pointer.DOKill();
            _spriteRenderer.DOFade(0f, 0.5f);
        }

        public bool IsActive { get; set; }
    }
}