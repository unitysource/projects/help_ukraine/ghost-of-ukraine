﻿// using System;
// using System.Collections;
// using System.Linq;
// using _Project.Scripts.Architecture.Services.UIStuff;
// using _Project.Scripts.UIStuff;
// using Assets._Project.Scripts.Utilities;
// using Assets._Project.Scripts.Utilities.Extensions;
// using DG.Tweening;
// using ModestTree;
// using UnityEngine;
//
// namespace _Project.Scripts.Mechanics
// {
//     public class PlaneCursorController : MonoBehaviour
//     {
//         [Header("Properties")] [SerializeField]
//         private float _scaleAnimateDelay;
//
//         [Header("Sub Behaviours")] [SerializeField]
//         private PlaneCursorUI _planeCursorUI;
//
//         [SerializeField] private PlaneCursor _cursorPrefab;
//
//         public Car CurrentTarget { get; private set; }
//         private PlaneCursor _cursor;
//         private Cars _cars;
//
//         private bool IsActive
//         {
//             get => _cursor.gameObject.activeSelf;
//             set => _cursor.gameObject.SetActive(value);
//         }
//
//         public bool CanChangeTarget { get; set; }
//
//         public void Construct(Cars cars, Tutorial tutorial, IUIService uiService)
//         {
//             _uiService = uiService;
//             _cars = cars;
//             _tutorial = tutorial;
//         }
//
//         public void Initialize()
//         {
//             CreateCursor();
//             IsActive = false;
//             _planeCursorUI.Initialize(this);
//             _uiService.AddListener(UIEventType.GameStartedAction, OnGameStarted);
//         }
//
//         private void OnGameStarted(Hashtable _)
//         {
//             DOVirtual.DelayedCall(2.5f, () =>
//             {
//                 CanChangeTarget = true;
//                 _tutorial.IsActive = true;
//             });
//         }
//
//         private void Update()
//         {
//             if (HaveTarget && IsActive)
//             {
//                 _cursor.transform.position = CurrentTarget.CursorPoint.position;
//             }
//         }
//
//         public void CursorClicked()
//         {
//             if (_tutorial.IsActive)
//                 _tutorial.Deactivate();
//
//             if (HaveTarget && CanChangeTarget)
//             {
//                 ChangeTarget();
//             }
//             else
//             {
//                 if (!_cars.AvailableCars.IsEmpty())
//                     SetTarget();
//             }
//         }
//
//         private void CreateCursor()
//         {
//             _cursor = Instantiate(_cursorPrefab);
//             _cursor.Initialize(_scaleAnimateDelay);
//         }
//
//         private void SetTarget()
//         {
//             CurrentTarget = _cars.AvailableCars.ToArray().GetRandomElement();
//             _cars.Ghost.CanAttack = true;
//             IsActive = true;
//
//             _cursor.ScaleAnimate();
//         }
//
//         private void ChangeTarget()
//         {
//             ChangeTargetAction();
//             CurrentTarget.IsTarget = false;
//             var targetPlane = _cars.GetNextCar(CurrentTarget);
//             targetPlane.IsTarget = true;
//             CurrentTarget = targetPlane;
//         }
//
//         public bool HaveTarget => CurrentTarget != null;
//
//         public void RemoveCursor()
//         {
//             CurrentTarget = null;
//             IsActive = false;
//         }
//
//         public Action ChangeTargetAction = () => { };
//         private Tutorial _tutorial;
//         private IUIService _uiService;
//     }
// }