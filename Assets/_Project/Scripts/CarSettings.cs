﻿using _Project.Scripts.Mechanics;
using UnityEngine;

namespace _Project.Scripts
{
    [CreateAssetMenu(fileName = "CarSettings", menuName = "CarSettings")]
    public class CarSettings : ScriptableObject
    {
        [SerializeField] private Sprite _wholeCar;
        [SerializeField] private Sprite _brokenCar;
        [SerializeField] private int _pointsAmount;
        
        public int PointsAmount => _pointsAmount;
        public Sprite BrokenCar => _brokenCar;
        public Sprite WholeCar => _wholeCar;
    }
}